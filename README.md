# PigLog
A simple, yet beautiful way to print stuff in the terminal.

![](https://gitlab.com/oglo-crates/piglog/-/raw/main/showcase.png)



# Examples:
```rust
use piglog::prelude::*;

fn main() {
  piglog::success!("Hello, world!"); // Something successfully did something!
  piglog::info!("Hello, world!"); // General information.
  piglog::warning!("Hello, world!"); // A warning.
  piglog::error!("Hello, world!"); // Something errored out, but the program can still run.
  piglog::fatal!("Hello, world!"); // Something errored out so bad, the program has to stop!
  piglog::note!("Hello, world!"); // Additional information, kind of like a hint.
  piglog::task!("Hello, world!"); // About something that the programmer has to implement.
  piglog::generic!("Hello, world!"); // A sub log message, basically, a way to continue another log message.

  piglog::piglog_config::enable_debug_log(); // Debug messages don't show up unless this function is called.

  piglog::debug!("Hello, world!"); // Debug log. (Extra log messages for nerds. Lol!)
}
```
