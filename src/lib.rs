#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_macros)]

use std::env;
use colored::{Colorize, ColoredString};

const PIGLOG_DEBUG_VAR_NAME: &str = "ENABLE_PIGLOG_DEBUG";

#[cfg_attr(feature = "clap_derive", derive(clap::Subcommand, clap::ValueEnum))]
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
pub enum LogMode {
    Success,
    Info,
    Note,
    Warning,
    Error,
    Fatal,
    Debug,
    Todo,
}

#[macro_export]
macro_rules! generic {
    ($($arg:tt)*) => ({
        log_generic_print(format!($($arg)*));
    });
}

#[macro_export]
macro_rules! success {
    ($($arg:tt)*) => ({
        log_core_print(format!($($arg)*), LogMode::Success);
    });
}

#[macro_export]
macro_rules! info {
    ($($arg:tt)*) => ({
        log_core_print(format!($($arg)*), LogMode::Info);
    });
}

#[macro_export]
macro_rules! error {
    ($($arg:tt)*) => ({
        log_core_print(format!($($arg)*), LogMode::Error);
    });
}

#[macro_export]
macro_rules! fatal {
    ($($arg:tt)*) => ({
        log_core_print(format!($($arg)*), LogMode::Fatal);
    });
}

#[macro_export]
macro_rules! warning {
    ($($arg:tt)*) => ({
        log_core_print(format!($($arg)*), LogMode::Warning);
    });
}

#[macro_export]
macro_rules! note {
    ($($arg:tt)*) => ({
        log_core_print(format!($($arg)*), LogMode::Note);
    });
}

#[macro_export]
macro_rules! debug {
    ($($arg:tt)*) => ({
        log_core_print(format!($($arg)*), LogMode::Debug);
    });
}

#[macro_export]
macro_rules! task {
    ($($arg:tt)*) => ({
        log_core_print(format!($($arg)*), LogMode::Todo);
    });
}

pub fn log_generic_print(msg: String) {
    println!("{} {}", " :".bright_black().bold(), msg);
}

pub fn log_core_print(msg: String, mode: LogMode) {
    match env::var(PIGLOG_DEBUG_VAR_NAME) {
        Ok(_) => (),
        Err(_) => {
            match mode {
                LogMode::Debug => return,
                _ => (),
            };
        },
    };

    let prefix_text: &str = match mode {
        LogMode::Success => "Success",
        LogMode::Info => "Info",
        LogMode::Note => "Note",
        LogMode::Error => "Error",
        LogMode::Fatal => "Fatal",
        LogMode::Warning => "Warning",
        LogMode::Debug => "Debug",
        LogMode::Todo => "TODO",
    };

    let prefix = apply_color(prefix_text.to_string(), &mode);

    let log_msg = format!("{left}{}{right} {}", prefix.bold(), msg, left = "[".bright_black().bold(), right = "] :".bright_black().bold());

    match mode {
        LogMode::Fatal => eprintln!("{}", log_msg),
        LogMode::Error => eprintln!("{}", log_msg),
        LogMode::Warning => eprintln!("{}", log_msg),
        _ => println!("{}", log_msg),
    };
}

fn apply_color(string: String, mode: &LogMode) -> String {
    let colored_string: ColoredString = match mode {
        LogMode::Success => string.bright_green(),
        LogMode::Info => string.bright_green(),
        LogMode::Note => string.bright_yellow(),
        LogMode::Warning => string.bright_yellow(),
        LogMode::Error => string.bright_red(),
        LogMode::Fatal => string.bright_red(),
        LogMode::Debug => string.bright_magenta(),
        LogMode::Todo => string.bright_cyan(),
    };

    return colored_string.to_string();
}

/// Imports the dependencies of the macros, but not the macros. To import the macros, import them directly from piglog::*
pub mod prelude {
    pub use super::{LogMode, log_generic_print, log_core_print};
}

/// Configuration for PigLog
pub mod piglog_config {
    use std::env;
    use super::PIGLOG_DEBUG_VAR_NAME;

    pub fn enable_debug_log() {
        env::set_var(PIGLOG_DEBUG_VAR_NAME, "");
    }

    pub fn disable_debug_log() {
        env::remove_var(PIGLOG_DEBUG_VAR_NAME);
    }
}
